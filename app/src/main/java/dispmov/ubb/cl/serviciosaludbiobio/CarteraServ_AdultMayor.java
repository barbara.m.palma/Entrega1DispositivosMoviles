package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CarteraServ_AdultMayor extends AppCompatActivity {

    private String url ="http://www.hospitallosangeles.cl/carteraservicios/cartera_de_servicios_2015.pdf";
    Button abrirNavegador, retroceder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartera_serv__adult_mayor);

        abrirNavegador = findViewById(R.id.abrirNavegador);
        abrirNavegador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        retroceder = findViewById(R.id.retroceder);
        retroceder.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(getApplicationContext(), MainAdultoMayorActivity.class);
                startActivity(iniciar);
                finish();
            }
        });
    }
}
