package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainAdultoMayorActivity extends AppCompatActivity {

    private MainAdultoMayorActivity _this = this;
    private int MODO_GENERAL = 1;

    private SharedPreferences sharedPre;
    private SharedPreferences.Editor editorSP;
    Button siguientePagina, cambiarModo;
    ImageView carteleraServicios, consultaPaciente, consultaHoraMedica, horarioVisita, farmaciaTurno, donarSangre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_adulto_mayor);

        sharedPre = getSharedPreferences(getString(R.string.sharedPreID), MODE_PRIVATE);
        editorSP = sharedPre.edit();

        donarSangre = findViewById(R.id.donacionSangre);
        carteleraServicios = findViewById(R.id.carteraDeServicio);
        consultaHoraMedica = findViewById(R.id.consultaHora);
        consultaPaciente = findViewById(R.id.consultarEstado);
        farmaciaTurno = findViewById(R.id.farmacia);
        horarioVisita = findViewById(R.id.horarioVisita);
        cambiarModo = findViewById(R.id.cambiarModo);
        siguientePagina = findViewById(R.id.saludResponde);

        siguientePagina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, SaludResponde_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        cambiarModo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editorSP.putInt("MODO",MODO_GENERAL);
                editorSP.commit();
                Intent iniciar = new Intent(_this, MainPublicoGeneralActivity.class);
                startActivity(iniciar);
                finish();
            }
        });

        donarSangre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, DonaSangre_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        carteleraServicios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, CarteraServ_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        horarioVisita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, Visitas_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        consultaHoraMedica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, ConsultaHora_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        consultaPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, ConsultarPaciente_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

        farmaciaTurno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iniciar = new Intent(_this, Farmacia_AdultMayor.class);
                startActivity(iniciar);
                finish();
            }
        });

    }
}
