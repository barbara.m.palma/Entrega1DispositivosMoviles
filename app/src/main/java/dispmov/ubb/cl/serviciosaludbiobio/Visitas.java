package dispmov.ubb.cl.serviciosaludbiobio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class Visitas extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private Visitas _this = this;
    private int MODO_ADULTO_M= 2;

    private SharedPreferences sharedPre;
    private SharedPreferences.Editor editorSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent iniciar;
        if (id == R.id.nav_cartera) {
            iniciar = new Intent(_this, CarteraServ.class);
            startActivity(iniciar);
            finish();
        } else if (id == R.id.nav_estado) {
            iniciar = new Intent(_this, ConsultarPaciente.class);
            startActivity(iniciar);
            finish();
        } else if (id == R.id.nav_hora) {
            iniciar = new Intent(_this, ConsultaHora.class);
            startActivity(iniciar);
            finish();
        } else if (id == R.id.nav_sangre) {
            iniciar = new Intent(_this, DonaSangre.class);
            startActivity(iniciar);
            finish();
        }else if(id == R.id.nav_farmacia){
            iniciar = new Intent(_this, Farmacia.class);
            startActivity(iniciar);
            finish();
        }else if(id == R.id.nav_visitas) {
            iniciar = new Intent(_this, Visitas.class);
            startActivity(iniciar);
            finish();
        }else if(id == R.id.nav_contacto) {
            iniciar = new Intent(_this, Contacto.class);
            startActivity(iniciar);
            finish();
        }else if (id == R.id.nav_fono) {
            iniciar = new Intent(_this, SaludResponde.class);
            startActivity(iniciar);
            finish();
        } else if (id == R.id.nav_modo) {
            editorSP.putInt("MODO",MODO_ADULTO_M);
            editorSP.commit();
            iniciar = new Intent(_this, MainAdultoMayorActivity.class);
            startActivity(iniciar);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
